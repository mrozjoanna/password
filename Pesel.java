package Zadania;

import java.util.Scanner;

public class Pesel {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer PESEL:");
        String pesel = scanner.nextLine();

        System.out.println("Is Pesel number correct? -> " + checkifLongEnough(pesel));
        System.out.println("Is control number correct? -> " + checkSum(pesel));
        System.out.println("Day of birth is -> " + getBirthDay(pesel));
        System.out.println("Month of birth is -> " + getBirthMonth(pesel));
        System.out.println("Year of birth is -> " + getBirthYear(pesel));
        System.out.println("Your sex is -> " + getSex(pesel));
    }

    public static boolean checkifLongEnough(String pesel) {
        if (pesel.length() != 11) return false;
        else return true;
    }

    public static boolean checkSum(String pesel) {
        int[] wagi = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        int suma = 0;
        for (int i = 0; i < 10; i++) {
            suma = suma + Integer.parseInt(pesel.substring(i, i + 1)) * wagi[i];
        }
        int cyfraKontrolna = Integer.parseInt(pesel.substring(10, 11));
        suma = suma % 10;
        suma = 10 - suma;
        suma = suma % 10;
        return (suma == cyfraKontrolna);
    }

    public static int getBirthDay(String pesel) {
        int day;
        day = 10 * Character.getNumericValue(pesel.charAt(4));
        day = day + Character.getNumericValue(pesel.charAt(5));
        return day;
    }

    public static int getBirthMonth(String pesel) {
        int month;
        month = 10 * Character.getNumericValue(pesel.charAt(2));
        month += Character.getNumericValue(pesel.charAt(3));
        if (month > 80 && month < 93) {
            month -= 80;
        } else if (month > 20 && month < 33) {
            month -= 20;
        } else if (month > 40 && month < 53) {
            month -= 40;
        } else if (month > 60 && month < 73) {
            month -= 60;
        }
        return month;
    }

    public static int getBirthYear(String pesel) {
        int year;
        int month;
        year = 10 * Character.getNumericValue(pesel.charAt(0));
        year = year + Character.getNumericValue(pesel.charAt(1));
        month = 10 * Character.getNumericValue(pesel.charAt(2));
        month += Character.getNumericValue(pesel.charAt(3));
        if (month > 80 && month < 93) {
            year += 1800;
        } else if (month > 0 && month < 13) {
            year += 1900;
        } else if (month > 20 && month < 33) {
            year += 2000;
        } else if (month > 40 && month < 53) {
            year += 2100;
        } else if (month > 60 && month < 73) {
            year += 2200;
        }
        return year;
    }

    public static String getSex(String pesel) {
        if (pesel.charAt(9) % 2 == 0) return "Kobieta";
        else return "Mężczyzna";
    }
}
